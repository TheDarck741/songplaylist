/* eslint-disable linebreak-style */
import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import KnobControl from './plugins/knobControl';
import router from './router';
import wb from './registerServiceWorker';
import { store } from './store/store';

Vue.prototype.$workbox = wb;
Vue.config.productionTip = false;

new Vue({
  router,
  vuetify,
  KnobControl,
  store,
  render: (h) => h(App),
}).$mount('#app');
