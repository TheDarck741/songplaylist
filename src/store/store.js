/* eslint-disable linebreak-style */
/* eslint-disable import/prefer-default-export */
/* eslint-disable indent */
/* eslint-disable no-new */
/* eslint-disable global-require */
/* eslint-disable no-undef */
/* eslint-disable eqeqeq */
/* eslint-disable no-param-reassign */
/* eslint-disable no-return-assign */

import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        nbMusic: 0,
        musicList: [['BEAST IN BLACK - Blind And Frozen (OFFICIAL VIDEO)', require('../assets/song.mp3'), require('../assets/blind_and_snow.jpg'), 'BEAST IN BLACK'],
        ['Fall Out Boy - The Young Blood Chronicles (Uncut Longform Video)', require('../assets/fall-out-boy-the-young-blood-chronicles-uncut-longform-video.mp3'), require('../assets/fallboys.jpg'), 'Fall Out Boy'],
        ['LINDEMANN - Ach so gern (One Shot Video)', require('../assets/lindemann-ach-so-gern-one-shot-video.mp3'), require('../assets/lindemann.jpg'), 'LINDEMANN'],
        ['Oomph! - Labyrinth', require('../assets/oomph-labyrinth.mp3'), require('../assets/oomph.jpg'), 'Oomph!'],
      ],
    },
    getters: {
        displayBack: (state) => {
            console.log(state.nbMusic);
            if (state.nbMusic == 0) {
              return state.nbMusic = 3;
            }
              return state.nbMusic - 1;
          },
          displayNext: (state) => {
            console.log(state.nbMusic);
            if (state.nbMusic == (state.musicList.length - 1)) {
              return state.nbMusic = 0;
            }
              return state.nbMusic + 1;
          },
    },
});
