/* eslint-disable import/no-unresolved */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-restricted-globals */
/* eslint-disable import/no-duplicates */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable import/no-mutable-exports */
/* eslint-disable no-console */

import { Workbox } from 'workbox-window';
import { CacheableResponsePlugin } from 'workbox-cacheable-response';
import { CacheFirst } from 'workbox-strategies';
import { ExpirationPlugin } from 'workbox-expiration';
import { registerRoute } from 'workbox-routing';

let wb;

if ('serviceWorker' in navigator) {
  wb = new Workbox(`${process.env.BASE_URL}service-worker.js`);

  wb.addEventListener('controlling', () => {
    window.location.reload();
  });

  // Cache images with a Cache First strategy
  registerRoute(
    // Check to see if the request's destination is style for an image
    ({ request }) => request.destination === 'image'
                    || request.destination === 'script'
                    || request.destination === 'style'
                    || request.destination === 'audioworklet'
                    || request.destination === 'audio',
    // Use a Cache First caching strategy
    new CacheFirst({
      // Put all cached files in a cache named 'assets'
      cacheName: 'assets',
      plugins: [
        // Ensure that only requests that result in a 200 status are cached
        new CacheableResponsePlugin({
          statuses: [200],
        }),
        // Don't cache more than 50 items, and expire them after 30 days
        new ExpirationPlugin({
          maxEntries: 50,
          maxAgeSeconds: 60 * 60 * 24 * 30, // 30 Days
        }),
      ],
    }),
  );

  console.log('serviceWorker');
  wb.register();
} else {
  wb = null;
}

export default wb;
